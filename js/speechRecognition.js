/**
* Author: Gustavo Conci
**/

window.Speech = function(selector, settings) {

	var recognition = new webkitSpeechRecognition(),
		transcript = [], interim = [], isFinal = false,
		target = document.getElementsByClassName(selector)[0];

	recognition.continuous = settings.continuous;
	recognition.interimResults = settings.interimResults;
	recognition.lang = settings.lang;

	var init = function() {

		recognition.start();
		recognition.onstart = function() {
			console.log('webkitSpeechRecognition: start!');
		};
		recognition.onerror = function(error) {
			console.log('webkitSpeechRecognition: error: ' + error);
		};
		recognition.onend = function() {
			console.log('webkitSpeechRecognition: end!');
		};
		recognition.onresult = function(event) {
			isFinal = false;

			for (var i = event.resultIndex; i < event.results.length; ++i) {
				if (event.results[i].isFinal) {
					isFinal = true;
					transcript.push(event.results[i][0].transcript);
				} else {
					interim = clone(transcript);
					interim.push('...');
				}
			}

			if (isFinal) target.value = transcript.join('');
			else target.value = interim.join('');
		};
	};

	function clone(obj) {
		if (obj === null || typeof obj != 'object')
			return obj;

		var temp = obj.constructor();

		for (var key in obj)
			if (obj.hasOwnProperty(key))
				temp[key] = clone(obj[key]);

		return temp;
}

	return init();

};