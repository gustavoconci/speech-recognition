$(function() {

	'use strict';

  $('html').removeClass('no-js');

  new Speech('speech', {
    continuous: true,
    interimResults: true,
    lang: 'pt-BR'
  });

});